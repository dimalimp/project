const initState = {
    footerItem: [
        { id: 0, title: "Заголовок", text: "Образец текста нижнего колонтитула" },
        { id: 1, title: "Заголовок", text: "Образец текста нижнего колонтитула" },
    ],
}

function reducer(state = initState, action) {
    switch (action.type) {
        case 'ADD_FOOTER_ITEM': {
            const newfooterItem = [...state.footerItem];
            newfooterItem.push(action.payload.footerItem);
            return {
                ...state,
                footerItem: newfooterItem,
            }
        }
        case 'DELETE_FOOTER': {
            const id = action.payload.id;
            const newFooter = [...state.footerItem];
            const delFooter = newFooter.filter(item => item.id !== id)
            return {
                ...state,
                footerItem: delFooter,
            }
        }

        default: return state;
    }
}

export default reducer;
