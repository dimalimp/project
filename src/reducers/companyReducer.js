const initState = {
    company: "ОТ ВИНТА",
}

function reducer(state = initState, action) {
    switch (action.type) {
        case "CREATE_COMPANY": {
            const name = action.payload.name
            return {
                ...state,
                company: (name === undefined ? action.payload.company : name),
            }
        }
        default: return state
    }
}

export default reducer;