import { combineReducers } from 'redux'
import popup from "./popapReducer"
import hotTours from "./hottoursReducer"
import company from "./companyReducer"
import footerItem from "./footerReducer"
import theme from "./themeReducer"

export default combineReducers({
    popup,
    hotTours,
    company,
    footerItem,
    theme,
})