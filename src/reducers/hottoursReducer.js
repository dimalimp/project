const initState = {
    showLoading: false,
    hotTours: [],
}

function reducer(state = initState, action) {
    switch (action.type) {
        case 'SHOW_FETCH_HOTTOURS': {
            return {
                ...state,
                showLoading: false,
                hotTours: action.payload.hotTours,
            }
        }
        case 'START_FETCH_HOTTOURS': {
            return { ...state, showLoading: true }
        }
        case 'END_FETCH_HOTTOURS': {
            return { ...state, showLoading: false }
        }
        case 'ADD_HOTTOUR': {
            const newHotTour = [...state.hotTours]
            const country = action.payload.country
            newHotTour.push(action.payload.hotTour)
            const newHotTourFirst = [...state.hotTours]
            return {
                ...state,
                hotTours: newHotTourFirst.filter(item => item.country !== country),
                hotTours: newHotTour,
            }
        }
        case 'EDIT_HOTTOUR': {
            const newHotTourFirst = [...state.hotTours]
            const country = action.payload.country
            return {
                ...state,
                hotTours: newHotTourFirst.filter(item => item.country !== country),
            }
        }
        case 'DELETE_HOTTOUR': {
            let newHotTour = [...state.hotTours]
            newHotTour = newHotTour.filter((hotTour) => hotTour.country !== action.payload.hotTour.country);
            return {
                ...state,
                hotTours: newHotTour,
            }
        }
        default: return state;
    }
}

export default reducer;