import { put, takeEvery } from 'redux-saga/effects';

import italy from "../assets/Images/HotImages/italy.jpeg"
import spain from "../assets/Images/HotImages/spain.jpeg"
import japan from "../assets/Images/HotImages/japan.jpeg"
import africa from "../assets/Images/HotImages/africa.jpeg"
import maldivs from "../assets/Images/HotImages/maldivs.jpg"
import france from "../assets/Images/HotImages/france.jpg"
import iceland from "../assets/Images/HotImages/iceland.jpg"

const delay = (ms) => new Promise((res) => setTimeout(res, ms));

export function* fetchHotTours(action) {
    yield put({ type: 'START_FETCH_HOTTOURS' });
    yield delay(500);

    const hotTours = [
        { id: 1, country: "Italy", image: italy, title: "300 £ + Скидки", text: "От древних культур до удивительных пейзажей - найдите самые лучшие скидки" },
        { id: 2, country: "Spain", image: spain, title: "Праздники Испании", text: "Высокие горы, залитые солнцем побережья, мавританское наследие и более изысканная кухня" },
        { id: 3, country: "Japan", image: japan, title: "Велосипедные Каникулы", text: "Пробудите давно потерянное в детстве чувство свободы или бросьте вызов приключениям" },
        { id: 4, country: "Africa", image: africa, title: "Праздники Африки", text: "Экзотические базары, древние чудеса, уникальная дикая природа и огромные песчаные дюны в бесконечных пустынях." },
        { id: 5, country: "Maldivs", image: maldivs, title: "Мальдивские пляжи", text: "Мальдивские пляжи всемирно известны своими белоснежными песками и кристально чистой водой." },
        { id: 6, country: "France", image: france, title: "Париж", text: "Самым посещаемым туристами городом мира является Париж, столица Франции." },
        { id: 7, country: "Iceland", image: iceland, title: "Природа Исландии", text: "Hезабываемое путешествие по самым интересным местам южной и западной Исландии с проживанием в отеле в Рейкьявике." },
    ];

    yield put({ type: 'SHOW_FETCH_HOTTOURS', payload: { hotTours } });
}

export default function* hotToursSaga() {
    yield takeEvery('FETCH_HOTTOURS', fetchHotTours);
}