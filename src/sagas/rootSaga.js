import { all } from 'redux-saga/effects';
import hotToursSaga from './hotToursSaga';

export default function* rootSaga() {
    yield all([hotToursSaga()]);
}