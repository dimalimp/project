import HeadSection from "../HeadSection";
import "./Gallery.scss"

import road from "./../../assets/Images/GalleryImages/road.jpeg"
import mounts from "./../../assets/Images/GalleryImages/mounts.jpeg"
import waterfall from "./../../assets/Images/GalleryImages/waterfall.jpeg"
import village from "./../../assets/Images/GalleryImages/village.jpeg"
import road_two from "./../../assets/Images/GalleryImages/road_two.jpeg"
import road_three from "./../../assets/Images/GalleryImages/road_three.jpeg"
import road_four from "./../../assets/Images/GalleryImages/road_four.jpeg"



const Gallery = () => {
    const galleryHead = {
        title: "Удивительные Места",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit nullam nunc justo sagittis suscipit ultrices."
    }
    const galleryImg = {
        road,
        mounts,
        waterfall,
        village,
        road_two,
        road_three,
        road_four
    }

    return (
        <section className="gallery">
            <div className="container">
                <HeadSection
                    className="gallery__headSection"
                    title={ galleryHead.title }
                    text={ galleryHead.text } />
                <div className="gallery__wrapper">
                    <div className="gallery__column">
                        <img className="gallery__image" src={ road }></img>
                        <img className="gallery__image" src={ waterfall }></img>
                    </div>
                    <div className="gallery__column">
                        <img className="gallery__image gallery__image_center" src={ mounts }></img>
                        <img className="gallery__image gallery__image_center" src={ village }></img>
                        <img className="gallery__image gallery__image_center" src={ road_two }></img>

                    </div>
                    <div className="gallery__column gallery__column_width">
                        <img className="gallery__image" src={ road_three }></img>
                        <img className="gallery__image" src={ road_four }></img>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Gallery;