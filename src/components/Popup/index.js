import { PureComponent } from "react";
import { closePopup } from "./../../actionCreators"
import { connect } from "react-redux";

import "./Popup.scss"

class Popup extends PureComponent {
    clickPopup = () => {
        const { closePopup } = this.props
        closePopup()
    }

    render() {
        const { showPopup } = this.props
        if (!showPopup) {
            return null
        }

        return (
            <div className="popup">
                <div className="popup__wrapper">
                    <p>Это заготовка попапа</p>
                    <button className="popup__close" onClick={ this.clickPopup }></button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({ popup }) => ({
    showPopup: popup.showPopup,
});

const mapDispatchToProps = dispatch => ({
    closePopup: () => dispatch(closePopup())
});

export default connect(mapStateToProps, mapDispatchToProps)(Popup);