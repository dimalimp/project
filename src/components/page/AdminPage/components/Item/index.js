import { PureComponent } from "react";

import "./Item.scss"
import Button from "../../../../Button";

class Item extends PureComponent {
    remove = () => {
        const { id, onRemove } = this.props;
        onRemove(id);
    };

    render() {
        const { text } = this.props;
        return (
            <div className="item">
                <Button className="button_orange" text="удалить" onClick={ this.remove } />
                <div className="item__text">{ text }</div>
            </div>
        );
    }
}

export default Item;