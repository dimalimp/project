import { connect } from 'react-redux';
import { PureComponent } from 'react';
import { addHotTour } from '../../../../../actionCreators';
import { editHotTour } from '../../../../../actionCreators';

import Input from '../../../../Input';
import Button from '../../../../Button';
import "./AddItem.scss"


class AddItem extends PureComponent {
    state = {
        country: '',
        image: '',
        title: '',
        text: '',
    };

    handleClick = () => {
        const { addHotTour } = this.props;
        const { editHotTour } = this.props;
        const { hotTours } = this.props;
        const { country, image, title, text } = this.state;
        hotTours.map((item) => { item.country === this.state.country ? (editHotTour(country)) : (console.log()) })
        const hotTour = {
            country,
            image,
            title,
            text,
            id: Math.floor(Math.random() * 100 + 10),
        };
        addHotTour(hotTour);
    }

    handleChangeCountry = (e) => {
        this.setState({ country: e.target.value });
    };

    handleChangeImage = (e) => {
        this.setState({ image: e.target.value })
    };

    handleChangeTitle = (e) => {
        this.setState({ title: e.target.value })
    };

    handleChangeText = (e) => {
        this.setState({ text: e.target.value })
    };

    render() {
        const { country, image, title, text } = this.state;
        return (
            <fieldset>
                <legend>Добавить/Редактировать блок</legend>
                <div className="admin-add">
                    <Input name="country" placeholder="country" onChange={ this.handleChangeCountry } className="input_mb" value={ country } />
                    <Input name="image" placeholder="image" onChange={ this.handleChangeImage } className="input_mb" value={ image } />
                    <Input name="text" placeholder="title" onChange={ this.handleChangeTitle } className="input_mb" value={ title } />
                    <Input name="text" placeholder="text" onChange={ this.handleChangeText } className="input_mb" value={ text } />
                </div>

                <div className="admin-add__btn">
                    <Button className="button_orange" text="добавить/редактировать" onClick={ this.handleClick } />
                </div>
            </fieldset>
        );
    }
}

const mapStateToProps = ({ hotTours }) => ({
    hotTours: hotTours.hotTours,
});

const mapDispatchToProps = (dispatch) => ({
    addHotTour: (hotTour) => dispatch(addHotTour(hotTour)),
    editHotTour: (country) => dispatch(editHotTour(country)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddItem);
