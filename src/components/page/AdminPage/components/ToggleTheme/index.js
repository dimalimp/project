import { PureComponent } from 'react';
import { connect } from 'react-redux';

import Button from "../../../../Button";
import { toggleTheme } from "../../../../../actionCreators"


class ToggleTheme extends PureComponent {
    state = {
        isShowMore: false,
        theme: ""
    }

    handleClick = () => {
        this.setState({ isShowMore: !this.state.isShowMore });
        this.state.isShowMore ? this.state.theme = "orange" : this.state.theme = "yellow";
        const { toggleTheme } = this.props;
        const { theme } = this.state;
        toggleTheme(theme);
    }

    render() {
        const { isShowMore } = this.state;
        const { theme } = this.props;
        return (
            <fieldset>
                <legend>Сменить тему</legend>
                <Button text={ isShowMore ? "жёлтая тема" : "оранжевая тема" } onClick={ this.handleClick }
                    className={ `button_${theme.theme}` } />
            </fieldset>
        )
    }
}

const mapStateToProps = ({ theme }) => ({
    theme: theme,
});

const mapDispatchToProps = (dispatch) => ({
    toggleTheme: (theme) => dispatch(toggleTheme(theme)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ToggleTheme);
