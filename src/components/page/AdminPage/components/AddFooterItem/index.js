import { connect } from 'react-redux';
import { PureComponent } from 'react';
import { addFooterItem } from "../../../../../actionCreators";

import Input from "../../../../Input";
import Button from "../../../../Button"

class AddFooterItem extends PureComponent {
    state = {
        title: '',
        text: '',
    };

    handleClick = () => {
        const { footerItem } = this.props;
        const { title, text } = this.state;
        const { addFooterItem } = this.props;
        const footerItems = {
            id: Math.floor(Math.random() * 10 + 2),
            title,
            text,
        };
        footerItem.length < 3 ? addFooterItem(footerItems) : console.log();
    }

    handleChangeTitle = (e) => {
        this.setState({ title: e.target.value })
    };

    handleChangeText = (e) => {
        this.setState({ text: e.target.value })
    };

    render() {
        const { title, text } = this.state;
        return (
            <fieldset>
                <legend>Добавить блок в подвале</legend>
                <Input name="text" placeholder="title" onChange={ this.handleChangeTitle } value={ title } />
                <Input name="text" placeholder="text" onChange={ this.handleChangeText } value={ text } />
                <Button className="button_orange" text="добавить" onClick={ this.handleClick } />
            </fieldset>
        );
    }
}

const mapStateToProps = ({ footerItem }) => ({
    footerItem: footerItem.footerItem,
});

const mapDispatchToProps = (dispatch) => ({
    addFooterItem: (footerItem) => dispatch(addFooterItem(footerItem)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddFooterItem);
