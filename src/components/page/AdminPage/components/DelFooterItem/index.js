import { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { deleteFooter } from "../../../../../actionCreators"

import Item from '../Item';

class DelFooterItem extends PureComponent {
    onItemRemove = id => {
        const { deleteFooter } = this.props;
        deleteFooter(id);
    };

    render() {
        const { footerItem } = this.props;
        return (
            <fieldset>
                <legend>Удалить блок в подвале</legend>
                { footerItem.map(item => (
                    <Item
                        id={ item.id }
                        text={ item.title }
                        onRemove={ this.onItemRemove }
                    />
                )) }
            </fieldset>
        )
    }
}

const mapStateToProps = ({ footerItem }) => ({
    footerItem: footerItem.footerItem,
});

const mapDispatchToProps = (dispatch) => ({
    deleteFooter: (footerItem) => dispatch(deleteFooter(footerItem)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(DelFooterItem));
