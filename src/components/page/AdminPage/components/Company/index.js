import { PureComponent } from "react";
import { connect } from "react-redux";
import { createNameCompany } from "../../../../../actionCreators"

import Input from "../../../../Input"
import Button from "../../../../Button"

class Company extends PureComponent {
    state = {
        name: "",
    };

    handleClick = () => {
        const { name } = this.state;
        const { createNameCompany } = this.props;
        createNameCompany(name);
    }

    handleChange = (e) => {
        this.setState({ name: e.target.value })
    };

    render() {
        const { name } = this.state;
        return (
            <fieldset>
                <legend>Изменить название компании</legend>
                <Input name="company" placeholder="company" onChange={ this.handleChange } value={ name } />
                <Button className="button_orange" text="название компании" onClick={ this.handleClick } />
            </fieldset>
        )
    }
}

const mapStateToProps = ({ company }) => ({
    company: company.company,
});

const mapDispatchToProps = (dispatch) => ({
    createNameCompany: (name) => dispatch(createNameCompany(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Company);