import { PureComponent } from 'react';
import { connect } from 'react-redux';
import { deleteHotTour } from '../../../../../actionCreators'

import './DelItem.scss';

import Button from '../../../../Button';
import Input from '../../../../Input';

class DeleItem extends PureComponent {
    state = {
        country: "",
    };

    handleClick = () => {
        const { deleteHotTour } = this.props;

        const { country } = this.state;
        const hotTour = {
            country,
        };
        deleteHotTour(hotTour);
    }

    handleChangeCountry = (e) => {
        this.setState({ country: e.target.value })
    };

    render() {
        const { country } = this.state;
        return (
            <fieldset>
                <legend>Удалить блок</legend>
                <div className="admin-del">
                    <Input name="country" placeholder="country" onChange={ this.handleChangeCountry } value={ country } />
                    <Button className="button_orange" text="удалить" onClick={ this.handleClick } />
                </div>
            </fieldset>
        );
    }
}

const mapStateToProps = ({ hotTours }) => ({
    hotTours: hotTours.hotTours,
});

const mapDispatchToProps = (dispatch) => ({
    deleteHotTour: (hotTour) => dispatch(deleteHotTour(hotTour)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DeleItem);

