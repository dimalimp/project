import { PureComponent } from "react";
import { withRouter } from 'react-router-dom';

import AddItem from "./components/AddItem"
import "./AdminPage.scss"
import Button from "../../Button";
import DelItem from "./components/DelItem";
import Company from "./components/Company";
import AddFooterItem from "./components/AddFooterItem";
import DelFooterItem from "./components/DelFooterItem";
import ToggleTheme from "./components/ToggleTheme";

class AdminPage extends PureComponent {
    handleClick = () => {
        this.props.history.push("/");
    }

    render() {
        return (
            <div className="admin">
                <div className="container">
                    <AddItem />
                    <DelItem />
                    <Company />
                    <AddFooterItem />
                    <DelFooterItem />
                    <ToggleTheme />
                    <div className="admin__btn">
                        <Button text="на главную" className="button_orange" onClick={ this.handleClick } />
                    </div>
                </div>
            </div>
        );
    }
}

export default (withRouter(AdminPage));