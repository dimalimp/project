import { Fragment, PureComponent } from 'react';

import Intro from '../../Intro';
import Features from '../../Features';
import HotTours from '../../HotTours';
import Gallery from '../../Gallery';
import Form from '../../Form';
import Statistics from '../../Statistics';
import Footer from '../../Footer'

class HomePage extends PureComponent {

    render() {
        return (
            <Fragment>
                <Intro />
                <Features />
                <HotTours />
                <Gallery />
                <Form />
                <Statistics />
                <Footer />
            </Fragment>
        )
    }
}

export default HomePage;