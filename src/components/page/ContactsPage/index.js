import { Fragment, PureComponent } from "react";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';


import Contacts from "../../Contacts";
import Form from "../../Form";
import HotItem from "../../HotItem"
import Intro from "../../Intro";
import Button from "../../Button";
import Footer from "../../Footer";
import "./ContactsPage.scss"

class ContactsPage extends PureComponent {
  componentDidMount() {
    window.scrollTo(0, 750)
  }

  handleClick = () => {
    this.props.history.push("/");
  }

  render() {
    const { hotTours } = this.props;
    const { theme } = this.props;
    return (
      <Fragment>
        <Intro />
        <Form />
        <Contacts />
        <div className="hot__inner">
          { hotTours.map((item) => (
            <HotItem
              key={ item.id }
              image={ item.image }
              title={ item.title }
              text={ item.text } />
          )) }
        </div>
        <div className="hot__btn">
          <Button className={ `button_${theme.theme}` } text="на главную" onClick={ this.handleClick } />
        </div>
        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = ({ hotTours, theme }) => ({
  hotTours: hotTours.hotTours,
  theme: theme,
});

export default connect(mapStateToProps)(withRouter(ContactsPage));
