import "./HotItem.scss"

const HotItem = ({ image, title, text }) => {
    return (
        <div className="hot-item">
            <img className="hot-item__img" src={ image }></img>
            <div className="hot-item__content">
                <h4 className="hot-item__title">{ title }</h4>
                <p className="hot-item__text">{ text }</p>
            </div>
        </div>
    )
}

export default HotItem;