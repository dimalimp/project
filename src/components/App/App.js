import { Fragment, PureComponent } from "react";
import { connect } from "react-redux";
import { Switch, Route, withRouter, Link } from 'react-router-dom';
import { fetchHotTours } from "../../actionCreators";

import Header from "../Header";
import Popup from "./../Popup";
import HomePage from "./../page/HomePage"
import ContactsPage from "./../page/ContactsPage"
import AdminPage from "./../page/AdminPage"

class App extends PureComponent {
  componentDidMount() {
    const { fetchHotTours } = this.props
    fetchHotTours()
  }

  render() {
    return (
      <Fragment>
        <Popup />
        <Header />
        <main>
          <Link to="/contacts" ></Link>
          <Link to="/admin" ></Link>
          <Link to="/"></Link>
          <Switch>
            <Route exact path="/contacts">
              <ContactsPage />
            </Route>
            <Route exact path="/admin">
              <AdminPage />
            </Route>
            <Route path="/">
              <HomePage />
            </Route>
          </Switch>
        </main>
      </Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchHotTours: () => dispatch(fetchHotTours()),
});


export default withRouter(connect(null, mapDispatchToProps)(App));
