import "./Button.scss"

const Button = ({ className = "", text, onClick, disabled }) => {
    return (
        <button onClick={ onClick } className={ `button ${className}` } disabled={ disabled }>{ text }</button>
    )
}

export default Button;