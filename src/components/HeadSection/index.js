import { connect } from "react-redux";

import Line from "../Line"
import "./HeadSection.scss"

const HeadSection = ({ title, text, className = "", theme }) => {
    return (
        <div className={ `headSection ${className}` }>
            <h2 className={ `headSection__title headSection__title_${theme.theme}` }>{ title }</h2>
            <Line className="line_mb" />
            <p className="headSection__text">{ text }</p>
        </div>
    )
}

const mapStateToProps = ({ theme }) => ({
    theme: theme,
});

export default connect(mapStateToProps)(HeadSection);