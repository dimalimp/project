import "./Input.scss"

const Input = ({ type, placeholder, value, className = "", onChange, name }) => {
    return (
        <input name={ name } className={ `input ${className}` } type={ type } placeholder={ placeholder } value={ value } onChange={ onChange }></input>
    )
}

export default Input;