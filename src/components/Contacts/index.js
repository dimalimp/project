import { connect } from "react-redux";

import "./Contacts.scss"

const Contacts = ({ theme }) => {
  return (
    <section className="contacts">
      <div className="container">
        <div className="contacts__wrapper">
          <h2 className={ `contacts__title contacts__title_${theme.theme}` }>Контакты</h2>
          <address className="contacts__address">
            <p className="contacts__text">Tel: +777 77 777 77 77</p>
            <p className="contacts__text">Email: travel@gmail.ru</p>
            <p className="contacts__text">г.Минск ул.Кирова, 28Б</p>
          </address>
        </div>
      </div>
    </section>
  )
}

const mapStateToProps = ({ theme }) => ({
  theme: theme,
});

export default connect(mapStateToProps)(Contacts);