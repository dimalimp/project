import { connect } from "react-redux";

import "./Line.scss"

const Line = ({ className = "", theme }) => {
    return (
        <div className={ `line ${className} line_${theme.theme} ` }></div>
    )
}

const mapStateToProps = ({ theme }) => ({
    theme: theme,
});

export default connect(mapStateToProps)(Line);