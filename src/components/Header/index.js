import { connect } from "react-redux";

import "./Header.scss"

const Header = ({ company, theme }) => {
    return (
        <header className="header">
            <div className="container">
                <h3 className="header__title">ТУРИСТИЧЕСКОЕ АГЕНТСТВО <span className={ `company_${theme.theme}` }>{ company }</span></h3>
            </div>
        </header>
    )
}

const mapStateToProps = ({ company, theme }) => ({
    company: company.company,
    theme: theme,
});

export default connect(mapStateToProps)(Header);