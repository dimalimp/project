import { PureComponent } from "react";
import { connect } from "react-redux";

import "./HotTours.scss";
import HeadSection from "../HeadSection";
import HotItem from "../HotItem";
import Button from "../Button"

class HotTours extends PureComponent {
    state = {
        isShow: false,
    }

    handleClick = () => {
        this.setState(({ isShow }) => ({ isShow: !isShow }));
    }

    render() {
        const hotHead = {
            title: "Горячие Направления",
            text: "Первое место для поиска экологически чистого отдыха"
        }

        const { hotTours } = this.props;
        const { theme } = this.props;
        const { isShow } = this.state;
        const allHotItem = hotTours;
        const smallHotItem = hotTours.slice(0, 4);
        const showHotItem = isShow ? allHotItem : smallHotItem;

        return (
            <section className="hot">
                <div className="container">
                    <HeadSection
                        className="hot__headSection"
                        title={ hotHead.title }
                        text={ hotHead.text } />
                    <div className="hot__inner">
                        { showHotItem.map((item) => (
                            <HotItem
                                key={ item.id }
                                { ...item } />
                        )) }
                    </div>
                    <div className="hot__btn">
                        { allHotItem.length <= 4 || <Button
                            className={ `button_${theme.theme}` }
                            text={ isShow ? "свернуть" : "показать больше" } onClick={ this.handleClick } /> }
                    </div>
                    <hr className="hot__line"></hr>
                </div>
            </section>
        )
    }
}

const mapStateToProps = ({ hotTours, theme }) => ({
    hotTours: hotTours.hotTours,
    theme: theme,
});

export default connect(mapStateToProps)(HotTours);