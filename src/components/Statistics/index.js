import { PureComponent } from "react";
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";

import "./Statistics.scss"
import HeadSection from "./../HeadSection/index"
import Button from "./../Button/index"
import StatItem from "./components/StatItem"

class Statistics extends PureComponent {
    handleClick = () => {
        this.props.history.push("/contacts");
    }

    render() {
        const headStatistics = {
            title: "Помогая Мечтам Исполняться",
            text: "Вы можете получить от нас столько помощи, сколько захотите. Тебе решать. Наконец, вы должны знать, что мы относимся к местным жителям с уважением и справедливостью. Это окупается загрузкой ведра, потому что заботливые местные жители позволяют ближе к их культуре, их людям и их природе. Что хорошо для них и хорошо для вас. Кроме того, если вы хотите, при бронировании отпуска мы оплатим однодневную поездку для малообеспеченного ребенка из развивающейся страны. в игровой парк, гору или музей и т. д. Мы называем это ответственным туризмом."
        }

        const arrayStat = [
            { id: 1, title: "ГОРЯЧИЕ НАПРАВЛЕНИЯ", num: 37 },
            { id: 2, title: "ДОВОЛЬНЫЕ КЛИЕНТЫ", num: 677 },
            { id: 3, title: "ЧАШКИ КОФЕ", num: 87 },
            { id: 4, title: "ПОСЕТИЛИ СТРАНЫ", num: 107 },
        ]

        const { theme } = this.props;

        return (
            <section className="statistics">
                <div className="container">
                    <HeadSection
                        className="statistics__headSection"
                        title={ headStatistics.title }
                        text={ headStatistics.text } />
                    <div className="statistics__wrapper">
                        <Button
                            className={ `button_orangeMb button_${theme.theme}` }
                            text="связаться" onClick={ this.handleClick } />
                        <div className="statistics__inner">
                            { arrayStat.map((item) => (
                                <StatItem
                                    key={ item.id }
                                    { ...item } />
                            )) }
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

const mapStateToProps = ({ theme }) => ({
    theme: theme,
});

export default connect(mapStateToProps)(withRouter(Statistics));