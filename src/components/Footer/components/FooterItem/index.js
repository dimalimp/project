import "./FooterItem.scss"

const FooterItem = ({ title, text }) => {
    return (
        <div className="footerItem">
            <h4 className="footerItem__title">{ title }</h4>
            <p className="footerItem__text">{ text }</p>
        </div>
    )
}

export default FooterItem;