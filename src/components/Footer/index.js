import { connect } from "react-redux";

import FooterItem from "./components/FooterItem";
import "./Footer.scss"

const Footer = ({ company, footerItem, theme }) => {
    return (
        <section className="footer">
            <div className="container">
                <div className="footer__content">
                    { footerItem.map((item) => (
                        <FooterItem
                            key={ item.id }
                            title={ item.title }
                            text={ item.text }
                        />
                    )) }
                </div>
                <hr className="footer__line"></hr>
                <h3 className="footer__title">Туристическая Компания <span className={ `company_${theme.theme}` }>{ company }</span></h3>
            </div>
        </section>
    )
}

const mapStateToProps = ({ company, footerItem, theme }) => ({
    company: company.company,
    footerItem: footerItem.footerItem,
    theme: theme,
})

export default connect(mapStateToProps)(Footer);