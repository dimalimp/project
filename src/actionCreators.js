export function openPopup() {
    return {
        type: "OPEN_POPUP",
    };
}

export function closePopup() {
    return {
        type: "CLOSE_POPUP",
    };
}

export function fetchHotTours() {
    return {
        type: "FETCH_HOTTOURS",
    };
}

export function addHotTour(hotTour) {
    return {
        type: 'ADD_HOTTOUR',
        payload: { hotTour },
    }
}

export function editHotTour(country) {
    return {
        type: 'EDIT_HOTTOUR',
        payload: { country },
    }
}

export function deleteHotTour(hotTour) {
    return {
        type: 'DELETE_HOTTOUR',
        payload: { hotTour },
    }
}

export function createNameCompany(name) {
    return {
        type: 'CREATE_COMPANY',
        payload: { name },
    }
}

export function addFooterItem(footerItem) {
    return {
        type: 'ADD_FOOTER_ITEM',
        payload: { footerItem }
    }
}

export function deleteFooter(id) {
    return {
        type: 'DELETE_FOOTER',
        payload: { id }
    }
}

export function toggleTheme(theme) {
    return {
        type: 'TOGGLE_THEME',
        payload: { theme }
    }
}